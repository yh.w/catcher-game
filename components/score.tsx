import db from './db/db';

export const scoreTb = 'scores';
export type ScoreType = {
  name: string;
  score: number;
  datetime: Date;
};

// As leaderboard table
export const rankTb = 'rankings';
export type RankType = {
  parent: string;
  score: number;
  snap: ScoreType;
};

const post = async (data: ScoreType) => {
  // Save record
  const row = await db.write(scoreTb, data) as any;

  // Update rankings
  const rankings = await db.read(rankTb) || {};
  const rows = Object.values(rankings) as any[];

  // binary search to get rank
  const searchIndex: any = (data: ScoreType[], x: number) => {
    if (data.length < 1) return 0;

    const mid = Math.floor((data.length - 1) / 2);
    if (data[mid].score == x) return mid;

    return data[mid].score > x
      ? mid + 1 + searchIndex(data.slice(mid + 1), x)
      : searchIndex(data.slice(0, mid), x)
    ;
  };
  const i = searchIndex(rows, row.score);

  // On index found
  if (i > -1) {
    const newRank: RankType = { parent: (row as any)._id, snap: row, score: row.score };
    rows.splice(i, 0, newRank); // older at behind
    await db.replace(rankTb, Object.assign({}, rows));
  }

  return {
    data: row,
    rank: i,
  };
};

const leaderboard = async () => {
  const rankings = await db.read(rankTb) || {};
  const rows = Object.values(rankings);
  // Return first 100
  return rows.slice(0, 100);
};

const score = {
  post,
  leaderboard,
};

export default score;
