import io from 'socket.io';
import score, { ScoreType } from '../score';

const handler = (socket: io.Socket) => {
  // create score
  const scoreCreate = async (obj: ScoreType) => {
    const created = await score.post(obj);
    socket.broadcast.emit('score.created', created);

    if (created.rank >= 0) {
      socket.broadcast.emit('leaderboard.sync', await score.leaderboard());
    }
  };
  socket.on('score.create', scoreCreate);

  const leaderboardGet = async () => {
    socket.broadcast.emit('leaderboard.sync', await score.leaderboard());
  };
  socket.on('leaderboard.get', leaderboardGet);
}; 

export default handler;
