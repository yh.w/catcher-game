import { JsonDB, Config } from 'node-json-db';
import { v4 as uuidv4 } from 'uuid';

// Load configs
const db = new JsonDB(new Config("db.json", true, false, '/'));
const handle = {
  read: async (path: string) => {
    try {
      const data = await db.getData(`/${path}`);
      return data;
    } catch (e) {
      return null;
    }
  },
  write: async (path: string, data: { [key: string]: any } | null = null) => {
    const uuid = uuidv4();
    await db.push(`/${path}/${uuid}`, data);
    return { _id: uuid, ...data };
  },
  replace: async (path: string, data: { [key: string]: any } | null = null) => {
    await db.push(`/${path}`, data);
    return data;
  },
  delete: async (path: string) => {
    const data = handle.read(path);
    await db.delete(`/${path}`);
    return (data as any)?._id || '';
  },
  clear: async () => await db.push('/', {}),
};

export default handle;
