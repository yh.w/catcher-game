// Configs, potenial in `.env`
const configs: any = {
  game: {
    time: 60, // game time, in s
    refresh: 20, // fps, in ms
  },
  handler: {
    ratio: 60 / 400, // image-to-screen ratio
    src: '/assets/boat.png',
    step: 20, // moving step size by key inputs
  },
  item: {
    ratio: 60 / 400, // image-to-screen ratio
    speed: 1,
    maxItems: 5, // max falling items on screen
    scores: { // scorings per type
      e: -100,
      p: 50,
    },
  },
};

// Constants
const font = '20px Arial';
const fillStyle = 'black';
const game = { // game init related
  dimension: { x: 400, y: 400 }, // default only
  initParams: () => ({
    score: 0,
    time: (configs?.game?.time || 60) * 1000, // timer, in (s)
    handler: { // catcher object
      x: 0,
      y: 0,
    },
    end: false, // game state
    items: [],
    itemsCollected: [],
  }),
};
let params: any = {}; // globally, storing live game stats
const callbacks = {
  onCompleted: (data: any) => {},
};

/*
 * Onboarding
 */

export const welcome = (
  ctx: CanvasRenderingContext2D,
  options: {
    dimension?: {
      x : number;
      y: number;
    };
    onCompleted?: any;
  } = {}
) => {
  // assign callback
  callbacks.onCompleted = typeof options.onCompleted == 'function' ? options.onCompleted : () => {};
  drawWelcome(ctx, options.dimension || null);
};

const drawWelcome = (
  ctx: CanvasRenderingContext2D,
  newDimension: {x : number; y: number;} | null = null
) => {
  // Apply dimension
  const dimension = {
    x: newDimension?.x || game.dimension.x,
    y: newDimension?.y || game.dimension.y,
  };
  ctx.clearRect(0, 0, dimension.x, dimension.y);

  const img = new Image();
  img.src = '/assets/bg2.png';
  img.onload = () => {
    ctx.beginPath();
    ctx.drawImage(
      img,
      0,
      0,
      ctx.canvas.clientWidth,
      ctx.canvas.clientHeight
    );

    // Print message
    ctx.font = font;
    ctx.fillStyle = 'white';
    ctx.fillText('Click to start!', dimension.x / 2 - 55, dimension.y / 2 - 10);

    // Listen onclick
    ctx.canvas.addEventListener('click', (e: MouseEvent) => {
      drawReset();
      start((e.target as any).getContext('2d'));
    });
  };
};

/*
 * Types
 */

type ParamsType = {
  ctx: CanvasRenderingContext2D;
  canvas: any;
  score: number;
  time: number;
  handler: {
    x: number;
    y: number;
  };
  end: boolean;
  items: ItemType[];
  itemsCollected: ItemType[];
};

export type ItemType = {
  name: string; // image name
  type: string;
  created: number; // created time
  y: number; // change with time
  x: number;
};

export type ResultType = {
  datetime: Date;
  score: number;
  collected: ItemType[];
};

/*
 * Drawings
 */
const start = (ctx: CanvasRenderingContext2D) => {
  // Init statistic
  const { canvas } = ctx;
  params = {
    ctx,
    canvas: ctx.canvas,
    ...game.initParams(),
  };

  // input event, on mousemove
  canvas.addEventListener('mousemove', (e: MouseEvent) => {
    if (params.end) return;

    params.handler.x = e.offsetX - canvas.offsetLeft;
  })

  // input event, on touchmove
  canvas.addEventListener('touchmove', (e: TouchEvent) => {
    if (params.end) return;

    params.handler.x = e.touches[0].clientX - canvas.offsetLeft;
  })

  // input event, on keydown
  window.addEventListener('keydown', (e: KeyboardEvent) => {
    if (params.end) return;

    const code = e.keyCode;
    if (code == 37) { // lefr
      params.handler.x -= (configs?.handler?.step || 20);
      if (params.handler.x < 0) params.handler.x = 0; // stop at edge left
    }
    if (code == 39) { // right
      params.handler.x += (configs?.handler?.step || 20);
      if (params.handler.x > canvas.clientWidth) params.handler.x = canvas.clientWidth; // stop at edge right
    }
  });

  // on time change, drawing
  setInterval(()=>{
    pushFallingItem();

    params.time -= (configs?.game?.refresh || 20);
    if (params.time < 0) params.end = true; 
    if (params.end) {
      drawGameOver();
      return;
    }

    draw();
  }, configs?.game?.refresh || 20);
};

// Main drawing handle, call once per fresh only
const draw = () => {
  params.ctx.clearRect(0, 0, params.canvas.width, params.canvas.height);

  drawBg();
  drawHandler()
  drawItems();
  drawPanel();
};

const drawBg = () => {
  const img = new Image();
  img.src = '/assets/bg1.png';
  params.ctx.drawImage(
    img,
    0,
    0,
    params.ctx.canvas.clientWidth,
    params.ctx.canvas.clientHeight
  );
};

const drawHandler = () => {
  // Draw image with the mouse pos
  const size = screenSize(configs?.handler?.ratio);
  const img = new Image();
  img.src = configs?.handler?.src || '';
  const { x } = params.handler;
  const y = (params.canvas.clientHeight - size) || 0; // img pos

  params.ctx.beginPath();
  params.ctx.drawImage(
    img,
    x - size / 2, // to stay middle
    params.ctx.canvas.clientHeight - size,
    size,
    size
  );
};

const drawItems = () => {
  for (let i = 0; i < params.items.length; i++) {
    const item = params.items[i];

    // Draw item
    const size = screenSize(configs?.item?.ratio);
    const img = new Image();
    img.src = `/assets/${item.name}.png`;
    item.y += configs?.item?.speed || 1;
    const { x, y } = item;

    const isCollides = collidesWith(item);
    if (isCollides) {
      params.score += configs?.item?.scores[item.type] || 0;
      params.itemsCollected.push(item);
    }

    params.ctx.beginPath();
    params.ctx.drawImage(img, x, y, size, size);

    // Remove out of canvas
    if (y > params.ctx.canvas.clientHeight || isCollides) params.items.splice(i, 1);
  }
};

const drawPanel = () => {
  params.ctx.font = font;
  params.ctx.fillStyle = fillStyle;
  params.ctx.fillText(`SCORE: ${params.score || 0}`, 10, 30);
  params.ctx.fillText(`Time: ${parseInt(((params?.time || 0) / 1000) as any)}s`, params.canvas.clientWidth - 95, 30);
};

const drawGameOver = () => {
  clearAllIntervals();

  const { canvas } = params.ctx;
  params.ctx.fillStyle = 'white';
  params.ctx.fillText('Click to restart!', canvas.clientWidth / 2 - 65, canvas.clientHeight / 2);

  callbacks.onCompleted({ datetime: new Date(), score: params.score, collected: params.itemsCollected });
};

const drawReset = () => {
  clearAllIntervals();
};

const clearAllIntervals = () => {
  for (var i = 1; i < 99999; i++) {
    window.clearInterval(i);
  }
};

/*
 * Game logics
 */
const pushFallingItem = () => {
  if (
    params.items.length < (configs?.item?.maxItems || 5) &&
    params.time % 800 === 0 // pushing interval, in ms
  ) {
    // Add items randomly
    const rand = Math.random();
    if (rand <= 0.66) {
      const type = rand <= 0.33 ? 'e' : 'p';
      const item = {
        name: `${type}${parseInt((Math.random() * (type == 'e' ? 2 : 4) + 1) as any)}`,
        type,
        created: params.time,
        y: 0,
        x: Math.random() * params.canvas.clientWidth,
      };

      params.items.push(item);
    }
  }
};

const collidesWith = (item: ItemType) => {
  const { handler } = params;

  // handler zone
  const handlerSizeReal = screenSize(configs?.handler?.ratio);
  const h = {
    x: {
      from: handler.x - handlerSizeReal / 2,
      to: handler.x + handlerSizeReal / 2,
    },
    y: {
      from: params.canvas.clientHeight - handlerSizeReal,
      to: params.canvas.clientHeight,
    },
  };

  // item zone
  const itemSizeReal = screenSize(configs?.item?.ratio);
  const i = {
    x: {
      from: item.x - itemSizeReal / 2,
      to: item.x + itemSizeReal / 2,
    },
    y: {
      from: item.y - itemSizeReal / 2,
      to: item.y + itemSizeReal / 2,
    },
  };

  return (
    // compare horizonal
    (
      (i.x.to >= h.x.from && i.x.from < h.x.to) ||
      (i.x.to > h.x.from && i.x.from <= h.x.to)
    ) &&
    // compare vertical
    i.y.to >= h.y.from
  );
};

/*
 * Helpers
 */
// Return real size on screen
const screenSize = (ratio: number = 0.4) => ratio * params.canvas.clientWidth;

const CatchGame = {
  welcome,
  context: {
    dimension: game.dimension,
  },
};

export default CatchGame;
