# Catch Game w/ Leaderboard 🍫

## Features

- A catch game
- Leaderboard

## System Design

- **Frontend**
  - Game

      1. Playground

      1. Scoring

  - Leaderboard

      1. Real-time rankings

- **Backend**
    1. `score.create` -> 1. craete 2. `leaderboard.sync`

    1. `leaderboard.get` -> show top 100

    1. `leaderboard.sync` -> send latest top 100

  *See [`apis.yml`](https://gitlab.com/yh.w/catcher-game/-/blob/master/apis.yml) for detailsed documentations. *

- **Database**
  
  See [`components/score.tsx`](https://gitlab.com/yh.w/catcher-game/-/blob/master/components/score.tsx):

  - `scoreTb`

  - `rankTb`

## Devpendencies

- **Next.js** https://nextjs.org/
- **Tailwind CSS** https://tailwindcss.com/
- **node-json-db** (as database) https://github.com/Belphemur/node-json-db
- **Socket.io** https://socket.io/

## Deployments

### Live Development Mode

`yarn run dev`

### Building for Production

- `yarn run tsc` Running Typescript compile
- `yarn run lint` Running Linting, to ensure **code standards**
- `yarn run build` Generating statis files for hosting, at `.next`

### Containerization with Docker

`docker build` with [`Dockerfile`](https://gitlab.com/yh.w/catcher-game/-/blob/master/Dockerfile)
