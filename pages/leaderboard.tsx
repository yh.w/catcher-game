import React, { useState, useEffect, useRef, useCallback } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { io, Socket } from 'socket.io-client';
import type { RankType } from '../component/score';
import btn from '../styles/Button.module.css';

let socket: Socket;

const Leaderboard: NextPage = () => {
  const [ranks, setRanks] = useState([]);

  const initSocket = useCallback(async () => {
    // call to init
    await fetch('/api/socket');

    socket = io();
    socket.emit('leaderboard.get', {
      ok: true,
    });
    socket.on('leaderboard.sync', (data) => {
      setRanks(data);
    });
  }, []);

  // On init
  useEffect(() => {
    initSocket();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <Head>
        <title>Catch Game Web Application</title>
      </Head>

      <main className="flex flex-col items-center">
        <div className="flex flex-col items-center my-[30px] py-4 border-b gap-y-4 w-full">
          <p className="text-center">Catch game Leaderboard</p>
          <div className="border w-full">
            <div className="border-b w-full flex">
              <div className="basis-[60%] text-center border-r p-3 font-bold">Name</div>
              <div className="basis-[40%] text-center p-3 font-bold">Score</div>
            </div>
            <div
              className="w-full min-h-[200px] max-h-[500px] overflow-y-scroll"
              style={{ background: 'url(/assets/bg2.png)' }}
            >
              <div>
              {ranks.length < 1 ? (
                <div className="text-center text-white p-5">
                  No data...
                </div>
              ) : (
                ranks.map((rank: RankType) => (
                  <div
                    key={rank.parent}
                    className="border-b w-full flex text-white"
                  >
                    <div className="basis-[60%] text-center border-r p-2">{rank.snap.name}</div>
                    <div className="basis-[40%] text-center p-2">{rank.score}</div>
                  </div>
                ))
              )}
              </div>
            </div>
          </div>
        </div>
        <Link href="/" className={`${btn.btn} grey border-grey cursor-pointer`}>
          Back to home
        </Link>
      </main>
    </div>
  );
};
export default Leaderboard;
