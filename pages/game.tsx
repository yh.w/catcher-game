import React, { useState, useEffect, useRef, useCallback } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { io, Socket } from 'socket.io-client';
import btn from '../styles/Button.module.css';
import CatchGame from '../components/catch-game';
import type { ResultType, ItemType } from '../components/catch-game';
import type { ScoreType } from '../components/score';

let socket: Socket;

type RankDataType = {
  data?: ScoreType;
  rank?: number | undefined;
};

const Game: NextPage = () => {
  const mainRef = useRef<HTMLElement>(null);
  const [dimension, setDimension] = useState({ x: 400, y: 400 });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const initEnded: {
    status: boolean;
    data: ResultType;
    name: string;
    error: string;
    rank: RankDataType;
  } = {
    status: false,
    data: {
      datetime: new Date(),
      score: 0,
      collected: [],
    },
    name: '',
    error: '',
    rank: {},
  };
  const [ended, setEnded] = useState(initEnded);

  const onCompleted = useCallback((data: ResultType) =>
    setEnded({
      ...ended,
      status: true,
      data,
    })
  , [setEnded, ended]);

  const onInput = (e: React.ChangeEvent<HTMLInputElement>) => 
    setEnded({
      ...ended,
      name: e.target.value,
    });

  const discard = useCallback(() => setEnded({ ...initEnded, rank: {} }), [setEnded, initEnded]);

  const submit = useCallback(() => {
    if (!ended.name) {
      setEnded({ ...ended, error: 'Please enter name!' });
      return;
    }

    socket.emit('score.create', {
      name: ended.name,
      score: ended.data.score,
      datetime: ended.data.datetime,
    });
  }, [ended, setEnded]);

  const submitCompleted = useCallback((rank: RankDataType) => {
    setEnded({ ...ended, status: true, rank });
  }, [setEnded, ended]);

  const initSocket = useCallback(async () => {
    // call to init
    await fetch('/api/socket');

    socket = io();
    socket.on('score.created', (data) => submitCompleted(data));
  }, [submitCompleted]);

  // On init
  useEffect(() => {
    // Get container dimension
    let size = mainRef?.current?.clientWidth || 400;
    // Allow max height/2 of width
    if (size > window.innerHeight) size = window.innerHeight / 2;
    setDimension({
      x: size,
      y: size,
    });

    initSocket();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // On `dimension` change
  useEffect(() => {
    // Get context
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

    // Init game
    CatchGame.welcome(ctx, { dimension, onCompleted });
  }, [dimension]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <Head>
        <title>Catch Game</title>
      </Head>

      <main
        ref={mainRef}
        className="flex flex-col items-center"
      >
        <div className="flex flex-col items-center my-[30px] py-4 border-b gap-y-4 relative">
          <canvas id="canvas" width={dimension?.x || 400} height={dimension?.y || 400}></canvas>
          <span className="text-rose-500">Hints: click once to restart</span>
          {ended.status === true ? (
            <div className="absolute top-1/4 w-full flex flex-col items-center p-3 border border-grey rounded-lg bg-white">
            {ended?.rank?.data ? (
              <>
                <p className="w-full text-center border-b mb-4">Saved!</p>
                <p>Your ranking is</p>
                <p className="text-5xl font-bold">{ended.rank.rank ? ended.rank.rank + 1 : null}</p>
                <div onClick={discard} className={`inline-block m-1 ${btn.btn} grey border-grey cursor-pointer`}>
                  Ok!
                </div>
              </>
            ) : (
              <>
                <p className="w-full text-center border-b mb-4">Game Ended!</p>
                <p>Your scores: {ended?.data?.score || 0}</p>
                <p>You caught:</p>
                <div className="caught-items">
                  {ended?.data?.collected?.map((v: ItemType, k: number) => (
                    <img
                      key={k}
                      className="inline-block w-[10%]"
                      src={`/assets/${v.name}.png`}
                      alt={`caught item ${k}.`}
                    />
                  ))}
                  {!ended?.data?.collected || ended.data.collected.length < 1 ? 'null' : null}
                </div>
                <div className="mb-4 text-center">
                  <p>Your name:</p>
                  <input
                    className="border"
                    type="text"
                    value={ended.name}
                    onChange={onInput}
                  />
                  {ended?.error ? <p className="text-rose-500">{ended.error}</p> : null}
                </div>
                <div className="actions text-center">
                  <div onClick={submit} className={`inline-block m-1 ${btn.btn} grey border-grey cursor-pointer`}>
                    Submit score
                  </div>
                  <div onClick={discard} className={`inline-block m-1 ${btn.btn} grey border-grey cursor-pointer`}>
                    Discard
                  </div>
                </div>
              </>
            )}
            </div>
          ) : null}
        </div>
        <Link href="/" className={`${btn.btn} grey border-grey cursor-pointer`}>
          Back to home
        </Link>
      </main>
    </div>
  );
};
export default Game;
