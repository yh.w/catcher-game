import type { NextApiRequest, NextApiResponse } from 'next';
import io, { Server } from 'socket.io';
import handler from '../../components/sockets/handler';

const SocketHandler = (req: NextApiRequest, res: NextApiResponse) => {
  const resSocket = res.socket as any;

  // if socket initialised
  if (resSocket.server.io) {
    res.end();
    return;
  }

  const server = new Server(resSocket.server);
  resSocket.server.io = server;

  const onConnection = (socket: io.Socket) => {
    handler(socket);
  };

  // handler as controller
  server.on('connection', onConnection);

  res.end();
};

export default SocketHandler;
