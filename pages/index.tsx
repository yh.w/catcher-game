import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';
import btn from '../styles/Button.module.css';

const Index: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Catch Game Web Application</title>
      </Head>

      <main className="flex flex-col items-center">
        <div className="flex flex-col items-center my-[30px] py-4 border-b gap-y-4">
          <Image src="/assets/boat.png" width="150" height="150" alt="boat" />
          <p className="text-center">Welcome to the Catch game, sandbox!</p>
        </div>
        <div className="inline-grid gap-y-4">
          <Link href="/game" className={`${btn.btn} blue border-blue cursor-pointer`}>
            Start Game
          </Link>
          <Link href="/leaderboard" className={`${btn.btn} purple border-purple cursor-pointer`}>
            Leaderboard
          </Link>
        </div>
      </main>
    </div>
  );
};
export default Index;
